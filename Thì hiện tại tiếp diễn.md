A. Cách Dùng

- Một việc đang diễn ra ở ngay tại thời điểm đang nói Eg: She is ** crying** in her bedroom.
- Nghĩa phàn nàn : dùng với always để diễn tả ý phàn nàn về một hành động thường lặp đi lặp lại Eg: He is always forgetting his book at home 
- Nghĩa tương lai : khi đã quyết định và sắp xếp làm một việc gì đó( một ngày hay một kế hoạch cố định) Eg: I am leaving tomorrow

B. Công Thức 

- Khẩng định :
  
  - S + am/is/are +V_ing
  - Chú Ý : I + am ; He/She/it + is; We/You/They + Are
 
- Câu phủ định:

 - S +am/is/are + V_ing
 - Chú Ý: am not không có dạng viết tắt is not = isn't, are not = aren't

- Câu hỏi :

 - Am/is/are + S + V_ing
 

Dấu hiệu: now, at the moment, right now, at the present, look, listen
